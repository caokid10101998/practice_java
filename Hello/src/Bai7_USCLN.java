import java.util.Scanner;

public class Bai7_USCLN {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Moi nhap so a: ");
        int a = sc.nextInt();
        System.out.print("Moi nhap so b: ");
        int b = sc.nextInt();
        System.out.println("USCLN của " + a + " và " + b + " là: " + USCLN(a, b));
        System.out.println("BSCNN của " + a + " và " + b + " là: " + BSCLN(a,b));

    }

    public static int USCLN(int a, int b) {
        int temp1 = a;
        int temp2 = b;
        while (temp1 != temp2) {
            if (temp1 > temp2) {
                temp1 = temp1 - temp2;
            } else {
                temp2 = temp2 - temp1;
            }
        }
        int uscln = temp1;
        return uscln;
    }
    public static int BSCLN(int a, int b){
        return (a*b)/USCLN(a,b);
    }

}
