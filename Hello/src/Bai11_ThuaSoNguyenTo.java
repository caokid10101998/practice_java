import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Bai11_ThuaSoNguyenTo {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("Mời nhập số nguyên n = ");
        int n = sc.nextInt();
        List<Integer> list = phanTichSoNguyen(n);
        System.out.printf("Kết quả: %d = ", n);
        int size = list.size();
        for (int i = 0; i < size - 1; i++) {
            System.out.print(list.get(i) + " x ");
        }
        System.out.print(list.get(size - 1));

    }

    public static List<Integer> phanTichSoNguyen (int n) {
        int i = 2;
        List<Integer> list = new ArrayList<Integer>();
        while (n > 1) {
            if (n % i == 0) {
                n = n / i;
                list.add(i);
            } else {
                i++;
            }
            if (list.isEmpty()) {
                list.add(n);
            }
        }
        return list;
    }

}
