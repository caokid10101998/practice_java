import java.util.Scanner;

public class Bai13_SoThuanNghich {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Mời nhập vào số n = ");
            int n = sc.nextInt();
            if (isThuanNghich(n)) {
                System.out.println("n là số thuận nghịch");
            } else {
                System.out.println("n không phải là số thuận nghịch");
            }
        } catch (Exception ex) {
            System.out.println("Mời nhập lại đầu vào là số nguyên: ");
        }
    }

    public static boolean isThuanNghich(int n) {
        String str = String.valueOf(n);
        int size = str.length();
        for (int i = 0; i < size / 2; i++) {
            if (str.charAt(i) != str.charAt(size - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
