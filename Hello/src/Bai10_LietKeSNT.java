public class Bai10_LietKeSNT {
    public static void main(String[] args) {
        for (int i = 10001; i < 99999; i+=2) {
            if (checkSoNguyenTo(i)) {
                System.out.println(i);
            }
        }
    }

    public static boolean checkSoNguyenTo(int n) {
        if (n < 2) {
            return false;
        }
        int m = (int) Math.sqrt(n);
        for (int i = 2; i <= m; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
