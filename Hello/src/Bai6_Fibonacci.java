import java.util.Scanner;

public class Bai6_Fibonacci {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Mời bạn nhập số n = ");
            int n = sc.nextInt();
            System.out.printf("%d Số Fibonacci đầu tiên là: ", n);
            for (int i = 0; i < n; i++) {
                System.out.print(checkFibonacci(i) + " ");
            }
        } catch (Exception ex) {
            System.out.println("Vui lòng nhập lại đầu vào là số!");
        }

    }

    public static int checkFibonacci(int n) {
        if (n < 0) {
           return -1;
        } else if ((n == 0) || (n == 1)) {
            return n;
        } else {
            return checkFibonacci(n - 1) + checkFibonacci(n - 2);
        }
    }
}

