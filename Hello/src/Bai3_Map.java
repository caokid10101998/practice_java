import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Bai3_Map {
    public static void main(String[] args) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        System.out.println("Moi ban nhap so vao: ");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int key = 1;
        while (key <= number) {
            map.put(key, key * key);
            key++;
        }
        System.out.println("Kêt qua la:" +map);
    }
}
