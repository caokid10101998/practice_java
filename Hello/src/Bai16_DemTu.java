import java.util.Scanner;
import java.util.StringTokenizer;

public class Bai16_DemTu {
    //    public static final char SPACE = ' ';
//    public static final char TAB = '\t';
//    public static final char BREAK_LINE = '\n';
//    public static void main (String []args){
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Mời bạn nhập thông tin ngẫu nhiên: ");
//        String n = sc.nextLine();
//        System.out.println("Số từ của chuỗi đã nhập là: "+ countWords(n));
//    }
//
//    public static int countWords (String input){
//        if (input == null){
//            return -1;
//        }
//        int count = 0;
//        int size = input.length();
//        for (int i = 0; i < size; i++) {
//            if (input.charAt(i) == SPACE || input.charAt(i) == TAB
//                    || input.charAt(i) == BREAK_LINE) {
//                    count+=0;
//            } else {
//            ++count;
//            }
//        }
//        return count;
//    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn nhập chuỗi: ");
        String str = scanner.nextLine();
        StringTokenizer stringTokenizer = new StringTokenizer(str);
        int count = stringTokenizer.countTokens();
        System.out.println("Số từ có trong chuỗi \"" + str + "\" = " + count);
    }
}
