import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Bai14_Fibonacci {

    public static void main(String[] args) {
//        List<Integer> list = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Mời nhập số nguyên n = ");
            int n = sc.nextInt();
            System.out.println("Các số Fibonacci nhỏ hơn n và là số nguyên tố là: ");
//            for (int i = 0; i < n; i++) {
//                list.add(checkFibonacci(i));
//            }
//            for (Integer x : list) {
//                if (checkSoNguyenTo(x)) {
//                    System.out.print(" " + x);
//                }
//            }
            int i = 0;
            while (checkFibonacci(i) < n) {
                int x = checkFibonacci(i);
                if (checkSoNguyenTo(x)) {
                    System.out.print(x + " ");
                }
                i++;
            }

        } catch (Exception ex) {
            System.out.println("Mời nhập lại đầu vào là số nguyên dương");
        }
    }

    public static int checkFibonacci(int n) {
        if (n < 0) {
            return -1;
        } else if ((n == 0) || (n == 1)) {
            return n;
        } else {
            return checkFibonacci(n - 1) + checkFibonacci(n - 2);
        }
    }

    public static boolean checkSoNguyenTo(int x) {
        if (x < 2) {
            return false;
        }
        int a = (int) Math.sqrt(x);
        for (int i = 2; i <= a; i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }
}
