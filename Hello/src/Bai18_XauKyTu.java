import java.util.Scanner;

public class Bai18_XauKyTu {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Mời bạn nhập chuỗi thứ 1: ");
        String str1 = sc.nextLine();
        System.out.println("Mời bạn nhập chuỗi thứ 2: ");
        String str2 = sc.nextLine();
        if (str1.contains(str2)){
            System.out.println("Chuỗi 2 có nằm trong chuỗi 1");
        }
        else {
            System.out.println("Chuỗi 2 không nằm trong chuỗi 1");
        }
    }
}
