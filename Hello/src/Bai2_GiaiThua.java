import java.util.Scanner;

public class Bai2_GiaiThua {
    public static void main(String[] args){
        System.out.println("Moi ban nhap so: ");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int i = 1;
        long giaithua = 1;
        while(i <= number){
            giaithua = giaithua * i;
            i++;
        }
        System.out.println("Giai thừa của " + number + " là: "  + giaithua);
    }
}