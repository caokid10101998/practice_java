import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
/*
Nhập một mảng số nguyên a0, a1, a2, ..., an-1.
In ra màn hình các phần tử xuất hiện trong mảng đúng 2 lần.
 */


public class Bai20_LietKeMang2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap so phan tu cua mang: ");
        int n = sc.nextInt();
        int[] arr = new int[n];
        System.out.println("Nhap cac phan tu cua mang: ");
        for (int i = 0; i < n; i++) {
            System.out.printf("a[%d]= ", i);
            arr[i] = sc.nextInt();
        }
        Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
        for (int i = 0; i < n; i++) {
            addElement(map, arr[i]);
        }
        System.out.println("Cac phan tu xuat hien 2 lan:");
        for (Integer key : map.keySet()) {
            if (map.get(key) == 2) {
                System.out.println(key + "");
            }
        }
    }

    public static void addElement(Map<Integer, Integer> map, int element) {
        if (map.containsKey(element)) {
            int count = map.get(element) + 1;
            map.put(element, count);
        } else {
            map.put(element, 1);
        }
    }
}
