import java.util.Scanner;
/*
Viết chương trình tính tổng của các chữ số của môt số nguyên n trong java.
Số nguyên dương n được nhập từ bàn phím. Với n = 1234, tổng các chữ số: 1 + 2 + 3 + 4 = 10
 */
public class Bai12_TongSoNguyen {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Mời bạn nhập số nguyên n = ");
        int n = sc.nextInt();
        System.out.printf("Tổng các chữ số của số nguyên %d là: %d", n, sumNumber(n));
    }
    public static int sumNumber(int n){
        int total = 0;
        do{
            total = total + n % 10;
            n=n/10;
        }
        while(n>0);
        return total;
    }
}